import { configureStore,getDefaultMiddleware } from '@reduxjs/toolkit';
import LossContributionGraphReducer from "../slices/loss-contribution-graph/LossContributionGraphSlice";
import TargetVsActualGraphReducer from "../slices/target-actual-graph/TargetVsActualGraphSlice";

{/*...For disable Thunk and add our custom middleware...*/}
// const middleware = [...getDefaultMiddleware({ thunk: false })];
export const store = configureStore({
  reducer: {
    LossContributionGraphReducer:LossContributionGraphReducer,
    TargetVsActualGraphReducer: TargetVsActualGraphReducer
  },
});
