import React from 'react';
import './App.css';
import Core from './core/Core';

function App() {
  return (
    <Core></Core>
  );
}

export default App;
