import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
const initialState = {
  data: 20,
  status: 'idle',
};
export const fetchGraphData = createAsyncThunk(
  'graph/fetchGraphData',
  ()=>{
    // ...Cors error handle by proxy in package.json 
    return axios.get('/downtime/breakdown_loss_contribution/?startDate=2021-05-01T18:30:00.000Z&endDate=2022-05-31T07:45:52.600Z&plantId=1').then(response =>{
        return response.data
    })
  }
);

export const LossContributionGraphSlice = createSlice({
  name: 'LossContributionGraphSlice',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setGraphData: (state) => {
      state.data =[];
    }
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchGraphData.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchGraphData.fulfilled, (state, action) => {
        state.status = 'idle';
        state.data = action.payload
      });
  },
});

export const { setGraphData } = LossContributionGraphSlice.actions;
export const selectCount = (state) => state.counter.data;


export default LossContributionGraphSlice.reducer;
