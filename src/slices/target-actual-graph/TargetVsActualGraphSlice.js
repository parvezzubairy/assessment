import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
const initialState = {
  data: 20,
  status: 'idle',
};

export const fetchTargetActualGraphData = createAsyncThunk(
  'graph/fetchTargetActualGraphData',
  ()=>{
    // ...Cors error handle by proxy in package.json
    return axios.get('/asset/target_vs_cleaned/?startDate=2022-03-31T18:30:00.000Z&endDate=2022-06-29T18:30:00.000Z&plantId=1').then(response =>{
        return response.data
    })
  }
);

export const TargetVsActualGraphSlice = createSlice({
  name: 'fetchTargetActualGraphData',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    setGraphData: (state) => {
      state.data =[];
    }
  },

  extraReducers: (builder) => {
    builder
      .addCase(fetchTargetActualGraphData.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchTargetActualGraphData.fulfilled, (state, action) => {
        state.status = 'idle';
        state.data = action.payload
      });
  },
});

export const { setGraphData } = TargetVsActualGraphSlice.actions;
export default TargetVsActualGraphSlice.reducer;
