import React from 'react';
import { Routes, Route, Navigate } from "react-router-dom";
import SideBar from '../pages/SideBar';

const RouterFlow = () => {
    const base_dataScience_url = '/services/data-science';
    return (
        <Routes>
            <Route path="/app/*" element={<SideBar />} />
            <Route path="/" element={<Navigate replace to="/app" />} />
            <Route path="*" element={<TempPage />} />
        </Routes>
    );
}
export const TempPage = () => {
    return (
        <h2 style={{ color: '#02174E', padding: '5rem', textAlign: 'center' }}>Coming Soon...</h2>
    )
}
export default RouterFlow;
