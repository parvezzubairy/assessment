import React from 'react';
import { Routes, Route, Navigate } from "react-router-dom";
import Dashboard from '../pages/dashboard/Dashboard';
import Menu from '../pages/menu/Main';

const InnerAppRouterFlow = () => {
    return (
        <Routes>
            <Route path="/menu" element={<Menu />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/" element={<Navigate replace to="/app/menu" />} />
        </Routes>
    );
}

export default InnerAppRouterFlow;