
import React from 'react';
import { useNavigate } from "react-router-dom";
import { Layout, Menu, theme } from 'antd';
import InnerAppRouterFlow from '../routes/InnerAppRouterFlow';
const { Header, Content, Footer, Sider } = Layout;
const items = [
  "Menu",
  "Dashboard"
].map((path, index) => ({
  key: String(path),
  label: `${path}`,
}));
const SideBar = () => {
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const navigate = useNavigate();

  const handleClick = ({ item, key, keyPath, domEvent }) => {
    navigate(`/app/${key.toLowerCase()}`)
  }
  return (
    <Layout hasSider>
      <Sider
        style={{
          overflow: 'auto',
          height: '80vh',
          // position: 're',
          // left: 0,
          // top: 0,
          // bottom: 0,
        }}
      >
        <div
          style={{
            height: 32,
            margin: 16,
            background: 'rgba(255, 255, 255, 0.2)',
          }}
        />
        <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']} items={items} onClick={handleClick} />
      </Sider>
      <Layout
        className="site-layout"
        style={{
          marginLeft: 10,
        }}
      >
        <Content
          style={{
            margin: '24px 16px 0',
            overflow: 'initial',
          }}
        >
          <InnerAppRouterFlow />
        </Content>
      </Layout>
    </Layout>
  );
};
export default SideBar;