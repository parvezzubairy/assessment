import React,{useEffect} from "react";
import { useSelector, useDispatch } from 'react-redux';
import {fetchTargetActualGraphData} from "../../../slices/target-actual-graph/TargetVsActualGraphSlice";
import {
  ComposedChart,
  Line,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

const TargetVsActualGraph = ()=> {

const graphData = useSelector(state=> state.TargetVsActualGraphReducer.data);
const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(fetchTargetActualGraphData());
  },[])
  
  let arr = (graphData && graphData.result) || []
  let avgDailyTargetMax = Math.max(...arr.map(item => item['AvgDailyTarget']));
  let totalModuleCleanedMax = Math.max(...arr.map(item => item['TotalModuleCleaned']));
  let maxOfYaxis = Math.max(avgDailyTargetMax,totalModuleCleanedMax);

  return (
    <ComposedChart
      width={500}
      height={400}
      data={graphData.result}
      margin={{
        top: 20,
        right: 10,
        bottom: 20,
        left: 10
      }}
    >
      <CartesianGrid strokeDasharray="3 3" stroke="#4e524f" vertical={false}  />
      <XAxis dataKey="Date" tickSize={5}  />
      <YAxis yAxisId="left" domain={[0,maxOfYaxis]} tickFormatter={(tick) => {
     return `${Math.round((tick/maxOfYaxis)*100)}%`;
     }}/>
      <YAxis  yAxisId="right"  orientation="right"/>
      <Tooltip />
      <Legend />
      <Bar yAxisId="left" dataKey="TotalModuleCleaned" barSize={40} fill="#413ea0" />
      <Bar yAxisId="left" dataKey="AvgDailyTarget" barSize={40} fill="#1aaaed"/>
      <Line yAxisId="right" type="monotone" dataKey="Deviation" stroke="#ff7300" />
    </ComposedChart>
  );
}
export default TargetVsActualGraph;
