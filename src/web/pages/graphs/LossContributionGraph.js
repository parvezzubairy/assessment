import React,{useEffect} from "react";
import { useSelector, useDispatch } from 'react-redux';
import {fetchGraphData} from "../../../slices/loss-contribution-graph/LossContributionGraphSlice";
import {
  ComposedChart,
  Line,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";


const LossContributionGraph = ()=> {
const graphData = useSelector(state=> state.LossContributionGraphReducer.data);
const dispatch = useDispatch();
  useEffect(()=>{
    dispatch(fetchGraphData());
  },[])
console.log("graph data...........",graphData)
  return (
    <ComposedChart
      width={500}
      height={400}
      data={graphData.result}
      margin={{
        top: 20,
        right: 20,
        bottom: 20,
        left: 0
      }}
    >
      <CartesianGrid strokeDasharray="3 3" stroke="#4e524f" vertical={false}  />
      <XAxis dataKey="FailureType" tickSize={1}  />
      <YAxis yAxisId="left" tickFormatter={(tick) => {
     return `${tick}%`;
     }}/>
      <YAxis  yAxisId="right"  orientation="right"/>
      <Tooltip />
      <Legend />
      <Bar yAxisId="left" dataKey="Loss" barSize={20} fill="#413ea0" />
      <Line yAxisId="right" type="monotone" dataKey="Frequency" stroke="#ff7300" />
    </ComposedChart>
  );
}
export default LossContributionGraph;
