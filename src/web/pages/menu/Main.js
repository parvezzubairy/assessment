import React from 'react';
import LossContributionGraph from "../graphs/LossContributionGraph";
import TargetVsActualGraph from '../graphs/TargetVsActualGraph';
import "../style.scss";
const Menu = () => {
    return (
        <div>
            <div className='graphs-wrapper'>
                <div className='loss-contribution-graph-wrapper'>
                    <div>Loss Contribution(%)</div>
                    <LossContributionGraph />
                </div>
                <div className='target-vs-actual-graph-wrapper'>
                    <div>Target Vs Actual</div>
                    <TargetVsActualGraph />
                </div>
            </div>
        </div>
    );
}

export default Menu;