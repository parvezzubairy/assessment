import React from 'react';
import { BrowserRouter } from "react-router-dom";
import Footer from '../web/pages/footer/Footer';
import Header from '../web/pages/header/Header';
import RouterFlow from '../web/routes/RouterFlow';
import ErrorBoundary from './ErrorBoundary';
function Core() {
    return (
        <BrowserRouter>
            <ErrorBoundary>
                <Header />
                <RouterFlow />
                <Footer />
            </ErrorBoundary>
        </BrowserRouter>
    );
}

export default Core;